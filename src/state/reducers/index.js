import * as actions from "../actions";

const reducer = (
  state = {
    isLoading: true,
    isAuthenticated: false,
    user: {},
    userManager: { users: [] },
  },
  action
) => {
  switch (action.type) {
    case actions.HA_GET_STATES_RESP:
      return { ...state, ha: action.payload };
    case actions.USER_LOGIN:
      return {
        ...state,
        isLoading: false,
        isAuthenticated: true,
        user: { ...action.payload },
      };
    case actions.USER_LOGOUT:
      return { ...state, isLoading: false, isAuthenticated: false, user: {} };
    case actions.UMGR_GET_USERS_RESPONSE:
      return {
        ...state,
        userManager: { ...state.userManager, users: action.payload },
      };
    case actions.UMGR_CREATE_USER_RESPONSE:
      debugger;
      return state;
    case actions.UMGR_DELETE_USER_RESPONSE:
      debugger;
      return state;
    case actions.UMGR_UPDATE_USER_RESPONSE:
      debugger;
      return state;
    default:
      return state;
  }
};

export default reducer;
