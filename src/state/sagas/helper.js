export const makeRequest = function* ({ token, method, url, body }) {
  try {
    const headers = {
      Accept: "application/json, application/xml, text/plain, text/html, *.*",
      "Content-Type": "application/json; charset=utf-8",
    };
    let request = {
      method: method,
      headers: token
        ? { ...headers, Authorization: "Bearer " + token }
        : headers,
      body: body
        ? typeof body === "string"
          ? body
          : JSON.stringify(body).replace(/\\u0000/g, "")
        : null,
    };
    if (!body || method === "GET") delete request.body;
    return yield fetch(url, request);
  } catch (e) {
    console.error("FATAL: Cannot reach url!");
    return e;
  }
};
