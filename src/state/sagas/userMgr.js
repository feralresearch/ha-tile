import { put, select } from "redux-saga/effects";
import * as actions from "state/actions.js";
import { makeRequest } from "./helper.js";

export const getUsers = function* (action) {
  const reduxState = yield select();

  const response = yield makeRequest({
    method: "GET",
    url: `${window._env_.REACT_APP_AUTH_SERVICE}/users`,
    token: reduxState.user.auth,
  });

  let payload = {};
  if (response && response.ok) {
    const res = yield response.json();
    payload = res;
  }

  yield put({ type: actions.UMGR_GET_USERS_RESPONSE, payload });
};

export const updateUser = function* (action) {
  yield console.log(action);
};

export const deleteUser = function* (action) {
  yield console.log(action);
};

export const createUser = function* (action) {
  yield console.log(action);
};

export const resetUser = function* (action) {
  yield console.log(action);
};
