import Delete from "./Delete";
import Password from "./Password";
import Update from "./Update";
import Help from "./Help";
import Validation from "./Validation";
export { Delete, Password, Update, Help, Validation };
