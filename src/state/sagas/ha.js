import { put, delay } from "redux-saga/effects";
import * as actions from "state/actions.js";
import { makeRequest } from "./helper.js";

export const getStates = function* (action) {
  const response = yield makeRequest({
    method: "GET",
    url: `${window._env_.REACT_APP_HA_API}/states`,
    token: window._env_.REACT_APP_HA_TOKEN,
  });

  let payload = {};
  if (response && response.ok) {
    const res = yield response.json();
    payload = {};
    res.forEach((entry) => {
      const entityType = entry.entity_id.split(".")[0];
      if (!payload[entityType]) payload[entityType] = [];
      payload[entityType].push(entry);
    });
  }

  yield put({ type: actions.HA_GET_STATES_RESP, payload });
};

export const turnOn = function* (entity_id) {
  yield _switch("turn_on", entity_id);
};

export const turnOff = function* (entity_id) {
  yield _switch("turn_off", entity_id);
};

const _switch = function* (endpoint, entity_id) {
  const response = yield makeRequest({
    method: "POST",
    url: `${window._env_.REACT_APP_HA_API}/services/switch/${endpoint}`,
    token: window._env_.REACT_APP_HA_TOKEN,
    body: { entity_id },
  });

  let payload = {};
  if (response && response.ok) {
    const res = yield response.json();
    payload = {};
    res.forEach((entry) => {
      const entityType = entry.entity_id.split(".")[0];
      if (!payload[entityType]) payload[entityType] = [];
      payload[entityType].push(entry);
    });
  }
  yield delay(250);
  yield put({ type: actions.HA_GET_STATES });
};
