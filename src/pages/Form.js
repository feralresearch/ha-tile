import {
  FIELDTYPE,
  FormElement,
  PopoverMenu,
  ClickToReveal,
  CopyToClipboard,
  WatchForOutsideClick,
} from "@theplacelab/ui";

import React, { useState } from "react";

const styles = {
  page: {
    textAlign: "left",
    width: "60rem",
    margin: "5rem auto 1rem auto",
  },
  element: {
    border: "0",
    display: "flex",
    alignItems: "row",
    width: "100%",
    maxWidth: "50rem",
  },
  col: {
    padding: "1rem",
    border: "0.2px solid lightgrey",
    display: "flex",
    flexAlign: "row",
    flexGrow: "0.5",
    width: "50%",
    maxWidth: "20rem",
  },
  row: {
    margin: "0 0 1rem 0",
  },
};

const Home = () => {
  const [formValues, setFormValues] = useState({});

  const onChange = (payload) => {
    setFormValues({ ...formValues, [payload.id]: payload.value });
  };

  return (
    <div style={styles.page}>
      <h1>ClickToReveal</h1>
      <ClickToReveal showHint={false}>This is Secret!</ClickToReveal>

      <h1>CopyToClipboard</h1>
      <CopyToClipboard content="Some Text To Copy to the clipboard">
        Click icon to copy to clipboard
      </CopyToClipboard>

      <h1>WatchForOutsideClick</h1>
      <WatchForOutsideClick onOutsideClick={() => {}}>
        [click outside me]
      </WatchForOutsideClick>

      <h1>Popover Menu</h1>
      <PopoverMenu
        items={[
          { name: "foo", icon: "dog", action: (e) => console.log(e) },
          { name: "cat", icon: "cat", action: (e) => console.log(e) },
          { name: "trash", icon: "trash", action: (e) => console.log(e) },
        ]}
      />
      <h1>Form Elements</h1>
      <form>
        <div>
          <div style={{ ...styles.element, border: 0 }}>
            <div style={{ flexGrow: "0.5" }}>
              <h2>Enabled</h2>
            </div>
            <div style={{ flexGrow: "0.5" }}>
              <h2>Disabled</h2>
            </div>
            <div style={{ flexGrow: "0.5" }}>
              <h2>Locked</h2>
            </div>
          </div>
        </div>
        {Object.keys(FIELDTYPE).map((type, idx) => {
          let value;
          let options;
          let data = { someData: "arbitrary json" };
          switch (type) {
            case FIELDTYPE.HIDDEN:
              return null;
            case FIELDTYPE.CHECKBOX:
            case FIELDTYPE.CHECKBOX_ICON:
              value = false;
              break;
            case FIELDTYPE.COORDINATE:
              value = { lat: 46.5197, lng: 6.6323 };
              break;
            case FIELDTYPE.TEXTAREA:
              value = "test *this*";
              break;
            case FIELDTYPE.TAGLIST:
              value = "dog,cat,woof";
              break;
            case FIELDTYPE.DATE:
              value = "1978-03-26";
              break;
            case FIELDTYPE.SLIDER:
              value = 0.5;
              options = {
                min: -1,
                max: 1,
                step: 0.01,
              };
              break;
            case FIELDTYPE.SELECTION:
            case FIELDTYPE.SELECT:
              value = 55;
              options = {
                selectionOptions: [
                  { name: "apple", id: 0 },
                  { name: "banana", id: 1 },
                  { name: "corvid", id: 234 },
                  { name: "doggo", id: 2 },
                  { name: "elephant", id: 55 },
                  { name: "fork", id: 23 },
                ],
                acceptsNew: true,
              };
              break;
            default:
              value = "test *this*";
          }

          if (value && !formValues[idx]) {
            setFormValues({ ...formValues, [idx]: value });
          }
          return (
            <div key={type} style={styles.row}>
              <div>
                <h3>{type}</h3>
              </div>
              <div style={styles.element}>
                <div style={styles.col}>
                  <FormElement
                    id={idx}
                    data={data}
                    label={type}
                    type={type}
                    value={formValues[idx]}
                    isDisabled={false}
                    options={options}
                    onChange={onChange}
                  />
                </div>
                <div style={styles.col}>
                  <FormElement
                    id={idx}
                    label={type}
                    type={type}
                    value={formValues[idx]}
                    isDisabled={true}
                    options={options}
                  />
                </div>
                <div style={styles.col}>
                  <FormElement
                    id={idx}
                    data={data}
                    label={type}
                    type={type}
                    value={formValues[idx]}
                    isDisabled={false}
                    options={options}
                    onChange={onChange}
                    isLockable={true}
                    isLocked={true}
                  />
                </div>
              </div>
            </div>
          );
        })}
      </form>
    </div>
  );
};
export default Home;
