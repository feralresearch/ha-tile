import React, { useState } from "react";
import { userLogin, userLogout } from "../../state/actions";
import { useParams } from "react-router-dom";
import { Validate } from "@theplacelab/auth";
import { useDispatch } from "react-redux";
import { SignIn } from "@theplacelab/auth";

const Validation = () => {
  const dispatch = useDispatch();
  let { token } = useParams();
  const [accountStatus, setAccountStatus] = useState(null);

  if (accountStatus) {
    return (
      <SignIn
        prefill={accountStatus}
        authService={window._env_.REACT_APP_AUTH_SERVICE}
        authHelper={window._env_.REACT_APP_AUTH_HELPER}
        onLogin={(payload) => dispatch(userLogin(payload))}
        onLogout={(payload) => dispatch(userLogout(payload))}
        onStatus={(status) => {
          const event = new Event("statusUpdate");
          event.data = {
            backgroundColor: "orange",
            icon: "cat",
            ...status,
          };
          window.dispatchEvent(event);
        }}
      />
    );
  } else {
    return (
      <div style={{ marginTop: "10rem" }}>
        <Validate
          token={token}
          onComplete={(accountStatus) => setAccountStatus(accountStatus)}
        />
      </div>
    );
  }
};
export default Validation;
