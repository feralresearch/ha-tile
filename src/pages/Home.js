import { useDispatch, useSelector } from "react-redux";
import React, { useEffect } from "react";
import { haGetStates, haOn, haOff } from "state/actions";

const styles = {
  page: {
    textAlign: "left",
    width: "60rem",
    margin: "5rem auto 1rem auto",
  },
  element: {
    border: "0",
    display: "flex",
    alignItems: "row",
    width: "100%",
    maxWidth: "50rem",
  },
  col: {
    padding: "1rem",
    border: "0.2px solid lightgrey",
    display: "flex",
    flexAlign: "row",
    flexGrow: "0.5",
    width: "50%",
    maxWidth: "20rem",
  },
  row: {
    margin: "0 0 1rem 0",
  },
  scene_switch_group: {
    paddingLeft: "2rem",
    display: "flex",
    flexAlign: "row",
    flexDirection: "row",
  },
};

const Switchable = ({ entity }) => {
  const dispatch = useDispatch();
  const isOn = entity.state === "on";
  return (
    <div
      onClick={() => {
        if (isOn) {
          dispatch(haOff(entity.entity_id));
        } else {
          dispatch(haOn(entity.entity_id));
        }
      }}
      key={`switch_${entity.entity_id}`}
      style={{
        display: "flex",
        background: isOn ? "yellow" : "white",
        border: `1px solid black`,
        opacity: isOn ? 1.0 : 0.5,
        width: "4rem",
        height: "4rem",
        overflow: "clip",
        fontSize: ".5rem",
        margin: ".2rem",
        alignItems: "center",
        alignContent: "center",
      }}
    >
      <div style={{ margin: "auto" }}>{entity.attributes?.friendly_name}</div>
    </div>
  );
};

const Home = () => {
  const reduxState = useSelector((redux) => redux);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(haGetStates());
  }, [dispatch]);

  return (
    <div style={styles.page}>
      <h2>Scene</h2>
      {reduxState.ha?.scene?.map((scene, idx) => {
        const sceneSatisfiedIfAll = scene.attributes.friendly_name
          .toLowerCase()
          .includes(" on")
          ? "on"
          : "off";
        const switches = [];

        let lastSwitchState;
        let switchesConsistent = true;
        scene.attributes.entity_id.forEach((entity, idx) => {
          if (entity.split(".")[0] === "switch") {
            const thisSwitch = reduxState.ha.switch.find(
              (sw) => sw.entity_id === entity
            );
            if (!lastSwitchState) lastSwitchState = thisSwitch.state;
            if (switchesConsistent && lastSwitchState !== thisSwitch.state)
              switchesConsistent = false;
            console.log(thisSwitch.state);
            switches.push(<Switchable entity={thisSwitch} />);
            lastSwitchState = thisSwitch.state;
          }
        });

        const sceneSatisfied =
          lastSwitchState === sceneSatisfiedIfAll && switchesConsistent;

        return (
          <div
            key={`scene_${idx}`}
            style={{
              marginTop: "1rem",
            }}
          >
            <div>
              {sceneSatisfied ? "🟢" : "🔴"}
              {scene.attributes.friendly_name} ({sceneSatisfiedIfAll} scene)
            </div>
            <div style={styles.scene_switch_group}>{switches}</div>
          </div>
        );
      })}

      <h2>Switches</h2>
      {reduxState.ha?.switch?.map((swtc, idx) => (
        <Switchable entity={swtc} />
      ))}
    </div>
  );
};
export default Home;
