// SortableTable.fixture.js
import React from "react";
import  {SortableTable}  from "@theplacelab/ui";

const headers =[
    { label: "Account", align: "left", width: "20rem", sortable:true },
    { label: "B"},
    { label: "C" },
    { label: "D", sortable:true  },
    { label: "E" },
    { label: "F" },
  ];

const data = [];
data.push([
      <div>
        a@a.com (An)
      </div>,
      <div>2</div>,
      <div>3</div>,
      <div>4</div>,
      <div>5</div>,
      <div style={{display:'flex',flexDirection:'row'}}>
        <div><input type="button" value="delete"/></div>
        <div><input type="button" value="go"/></div>
      </div>,
    ]);
  data.push([
    <div>
      zzzzz@zz.net (ZZZZZ)
    </div>,
    <div>7</div>,
    <div>8</div>,
    <div>9</div>,
    <div>10</div>,
    <div>11</div>,
  ]);  data.push([
    <div>
      b@b.com (Bo)
    </div>,
    <div>7</div>,
    <div>8</div>,
    <div>9</div>,
    <div>10</div>,
    <div>11</div>,
  ]);
    data.push([
    <div>
      c@cat.com (Cat Cat)
    </div>,
    <div>7</div>,
    <div>8</div>,
    <div>9</div>,
    <div>10</div>,
    <div>11</div>,
  ]);

export default <SortableTable headers={headers} data={data} options={{searchOn:0,filterOn:0}}/>;
