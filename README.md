# HA Tile

React front-end for Home Assistant, specifically for making buttons which toggle or cycle scenes. Does not support every HA feature, nor does it intend to - but feel free to branch and expand! Uses the [HA REST API](https://developers.home-assistant.io/docs/api/rest/)

## HA Setup

- Setup HA and configure one or more scenes.
- HA doesn't ship with a working CORS configuration, you will need to do that by changing the config.ymal http option. Assuming you are at home and running this behind a firewall, you are probably not scared of cross-origin attacks, so permissive CORS is fine:

/config/configuration.yaml

```
http:
  cors_allowed_origins:
    - "*"
```

## Config

Setup your API endpoint and long running token in your environment variables
